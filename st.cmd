# require statments should only specify the module name
# The version of the module shall be defined in the environment.yaml file
# Note that all modules shall be in lowercase
## Add extra modules here
require essioc

iocshLoad("$(essioc_DIR)/common_config.iocsh")

epicsEnvSet(EPICS_DB_INCLUDE_PATH, "$(E3_CMD_TOP)/db:$(EPICS_DB_INCLUDE_PATH=.)")

require s7plc, 1.1.0

#require s7plc, 1.4.1

#require s7plc, 1.4.1-199d793



# Set EPICS environment variables
## Edit these as required for specific IOC requirements
## General IOC environment variables

#### IN THE PREFIX MUST BE REPLACED CRYO to Cryo !!!!!!!!!!!!!!!!!!
epicsEnvSet("PREFIX", "CrS-ACCP:CRYO-")
epicsEnvSet("IPADDR", "172.16.114.20")
#epicsEnvSet("DEVICE", "EDIT: change this")
#epicsEnvSet("LOCATION", "EDIT: change this")
#epicsEnvSet("ENGINEER", "EDIT: Name <email@esss.se>"
## Add extra environment variables here



######### PLC to IOC

dbLoadRecords("header_sysPLCtoIOC.db", "PREFIX=${PREFIX}, PLC_NAME=DB1101")
dbLoadRecords("DB1100.db", "PREFIX=${PREFIX}, PLC_NAME=DB1100")
dbLoadRecords("DB1102.db", "PREFIX=${PREFIX}, PLC_NAME=DB1102")
dbLoadRecords("DB1103.db", "PREFIX=${PREFIX}, PLC_NAME=DB1103")
dbLoadRecords("DB1104.db", "PREFIX=${PREFIX}, PLC_NAME=DB1104")
dbLoadRecords("DB1106.db", "PREFIX=${PREFIX}, PLC_NAME=DB1106")
dbLoadRecords("DB1107.db", "PREFIX=${PREFIX}, PLC_NAME=DB1107")
dbLoadRecords("DB1108.db", "PREFIX=${PREFIX}, PLC_NAME=DB1108")
dbLoadRecords("DB1110.db", "PREFIX=${PREFIX}, PLC_NAME=DB1110")
dbLoadRecords("DB1112.db", "PREFIX=${PREFIX}, PLC_NAME=DB1112")
dbLoadRecords("DB1114.db", "PREFIX=${PREFIX}, PLC_NAME=DB1114")
dbLoadRecords("DB1116.db", "PREFIX=${PREFIX}, PLC_NAME=DB1116")
dbLoadRecords("DB1118.db", "PREFIX=${PREFIX}, PLC_NAME=DB1118")
dbLoadRecords("DB1120.db", "PREFIX=${PREFIX}, PLC_NAME=DB1120")
dbLoadRecords("DB1122.db", "PREFIX=${PREFIX}, PLC_NAME=DB1122")

######### IOC to PLC

dbLoadRecords("header_sysIOCtoPLC.db", "PREFIX=${PREFIX}, PLC_NAME=DB1133, PORT=2012, BYTES=20")

dbLoadRecords("DB1132.db", "PREFIX=${PREFIX}, PLC_NAME=DB1132")
dbLoadRecords("DB1134.db", "PREFIX=${PREFIX}, PLC_NAME=DB1134")
dbLoadRecords("DB1136.db", "PREFIX=${PREFIX}, PLC_NAME=DB1136")
dbLoadRecords("DB1142.db", "PREFIX=${PREFIX}, PLC_NAME=DB1142")
dbLoadRecords("DB1144.db", "PREFIX=${PREFIX}, PLC_NAME=DB1144")
dbLoadRecords("DB1146.db", "PREFIX=${PREFIX}, PLC_NAME=DB1146")
dbLoadRecords("DB1148.db", "PREFIX=${PREFIX}, PLC_NAME=DB1148")
dbLoadRecords("DB1150.db", "PREFIX=${PREFIX}, PLC_NAME=DB1150")
dbLoadRecords("DB1152.db", "PREFIX=${PREFIX}, PLC_NAME=DB1152")

######### Alarm line
dbLoadRecords("DB1100alarms.db", "PREFIX=${PREFIX}")
dbLoadRecords("DB1102alarms.db", "PREFIX=${PREFIX}")
dbLoadRecords("DB1103alarms.db", "PREFIX=${PREFIX}")
dbLoadRecords("DB1104alarms.db", "PREFIX=${PREFIX}")
dbLoadRecords("DB1107alarms.db", "PREFIX=${PREFIX}")
dbLoadRecords("DB1108alarms.db", "PREFIX=${PREFIX}")
dbLoadRecords("DB1110alarms.db", "PREFIX=${PREFIX}")
dbLoadRecords("DB1112alarms.db", "PREFIX=${PREFIX}")
dbLoadRecords("DB1114alarms.db", "PREFIX=${PREFIX}")
dbLoadRecords("DB1116alarms.db", "PREFIX=${PREFIX}")
dbLoadRecords("DB1118alarms.db", "PREFIX=${PREFIX}")
dbLoadRecords("DB1120alarms.db", "PREFIX=${PREFIX}")
dbLoadRecords("DB1122alarms.db", "PREFIX=${PREFIX}")

######### Load to PLC
dbLoadRecords("load_to_plc.db", "PREFIX=${PREFIX}")
dbLoadRecords("import_data.db", "PREFIX=${PREFIX}, PLC_NAME=DB1132")
dbLoadRecords("import_data.db", "PREFIX=${PREFIX}, PLC_NAME=DB1134")
dbLoadRecords("import_data.db", "PREFIX=${PREFIX}, PLC_NAME=DB1136")
dbLoadRecords("import_data.db", "PREFIX=${PREFIX}, PLC_NAME=DB1144")
dbLoadRecords("import_data.db", "PREFIX=${PREFIX}, PLC_NAME=DB1146")
dbLoadRecords("import_data.db", "PREFIX=${PREFIX}, PLC_NAME=DB1148")
dbLoadRecords("import_data.db", "PREFIX=${PREFIX}, PLC_NAME=DB1150")
dbLoadRecords("import_data.db", "PREFIX=${PREFIX}, PLC_NAME=DB1152")
dbLoadRecords("direct_load.db", "PREFIX=${PREFIX}")


######### Restore from PLC
dbLoadRecords("last_alarm.db", "PREFIX=${PREFIX}")
dbLoadRecords("restore_from_plc.db", "PREFIX=${PREFIX}")
dbLoadRecords("RBtoSP.db", "PREFIX=${PREFIX}")

######### SCREENLOCK
dbLoadRecords("ScreenLock.db", "PREFIX=${PREFIX}")
dbLoadRecords("ScreenLockLocal.db", "PREFIX=${PREFIX}, LOCKID=ONE")
dbLoadRecords("ScreenLockLocal.db", "PREFIX=${PREFIX}, LOCKID=TWO")
dbLoadRecords("ScreenLockLocal.db", "PREFIX=${PREFIX}, LOCKID=LCR1")

######### Man Auto
dbLoadRecords("manAutoAnalogue.db", "PREFIX=${PREFIX}")
dbLoadRecords("manAutoDigital.db", "PREFIX=${PREFIX}")

#s7plcConfigure (PLCname, IPaddr, port, inSize[B], outSize[B], bigEndian, recvTimeout[ms], sendInterval[ms])
s7plcConfigure("DB1100", "${IPADDR}", 2000, 1982,    0, 1,  500,  100)
s7plcConfigure("DB1101", "${IPADDR}", 2011,   19,    0, 1,  500,  100) # system
s7plcConfigure("DB1102", "${IPADDR}", 2001, 1266,    0, 1,  500,  100)
s7plcConfigure("DB1103", "${IPADDR}", 2002,   58,    0, 1,  500,  100)
s7plcConfigure("DB1104", "${IPADDR}", 2003,  204,    0, 1,  500,  100)
s7plcConfigure("DB1106", "${IPADDR}", 2004, 3086,    0, 1, 5000, 1000)
s7plcConfigure("DB1107", "${IPADDR}", 2007, 2112,    0, 1, 5000, 1000)
s7plcConfigure("DB1108", "${IPADDR}", 2017,  332,    0, 1,  500,  100)
s7plcConfigure("DB1110", "${IPADDR}", 2018,  448,    0, 1,  500,  100)
s7plcConfigure("DB1112", "${IPADDR}", 2019,   58,    0, 1,  500,  100)
s7plcConfigure("DB1114", "${IPADDR}", 2008,  590,    0, 1, 5000, 1000)
s7plcConfigure("DB1116", "${IPADDR}", 2013, 1372,    0, 1, 5000, 1000)
s7plcConfigure("DB1118", "${IPADDR}", 2014,  936,    0, 1, 5000, 1000)
s7plcConfigure("DB1120", "${IPADDR}", 2027, 1284,    0, 1,  500,  100)
s7plcConfigure("DB1122", "${IPADDR}", 2029,  598,    0, 1, 5000, 1000)

s7plcConfigure("DB1132", "${IPADDR}", 2005,    0,  920, 1,  500,  100)
s7plcConfigure("DB1133", "${IPADDR}", 2012,    0,   20, 1,  500,  100) # system
s7plcConfigure("DB1134", "${IPADDR}", 2006,    0,  112, 1,  500,  100)
s7plcConfigure("DB1136", "${IPADDR}", 2009,    0, 3086, 1, 5000, 1000)
s7plcConfigure("DB1142", "${IPADDR}", 2026,    0,   36, 1,  500,  100)
s7plcConfigure("DB1144", "${IPADDR}", 2010,    0,  360, 1, 5000, 1000)
s7plcConfigure("DB1146", "${IPADDR}", 2015,    0, 1042, 1, 5000, 1000)
s7plcConfigure("DB1148", "${IPADDR}", 2016,    0,  502, 1, 5000, 1000)
s7plcConfigure("DB1150", "${IPADDR}", 2028,    0,  324, 1,  500,  100)
s7plcConfigure("DB1152", "${IPADDR}", 2030,    0,  252, 1, 5000, 1000)


# Call iocInit to start the IOC
iocInit()

## Add any post-iocInit statements here

