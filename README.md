# e3-Crs_ACCP_Ctrl_IOC_1


**IMPORTANT !!!!**

This Repo should be used only to hold the **substitutions** and **template** files under the **subs** folder, 
the **.db** files in the **db** folder, and the **st.cmd** file
When upgraded, this last file and/or  **.db** files, should be then copied to the deployment IOC repo. 

The Repo for deployment is in 

https://gitlab.esss.lu.se/iocs/crs/e3-ioc-crs-accp

where the db files should be synchronized with the ones created in this git repo.
